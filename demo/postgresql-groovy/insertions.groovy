@Grapes(
    @Grab(group='org.postgresql', module='postgresql', version='42.7.3')
)
@GrabConfig(systemClassLoader = true)
import groovy.sql.Sql

def url = 'jdbc:postgresql://localhost:5432/groovy_db'
def usr = 'groovy_usr'
def pwd = 'groovy_pwd'
def drv = 'org.postgresql.Driver'

Sql.withInstance(url, usr, pwd, drv) {db ->
    db.execute '''
       DROP TABLE IF EXISTS persons;
       CREATE TABLE persons (
            id    SERIAL PRIMARY KEY,
            name  TEXT NOT NULL,
            age   INTEGER NOT NULL
        );
    '''
    
    def r = new Random()
    def names = ['Anna','Berit','Carin','Doris','Eva','Frida']
    def SQL = 'INSERT INTO persons (name, age) VALUES ' +
               names.collect { "('${it}', ${20 + r.nextInt(60)})" }.join(', ')
    println 'SQL: ' + SQL
    def rows = db.executeInsert(SQL)
    println 'ROWS: '+rows
    
    SQL = '''UPDATE persons SET age = 42 WHERE name LIKE '%i%' '''
    println '----\nSQL: ' + SQL
    def n = db.executeUpdate SQL
    println "${n} rows updated"
    
    println 'ROWS: '+db.rows('SELECT name,age FROM persons')
}

