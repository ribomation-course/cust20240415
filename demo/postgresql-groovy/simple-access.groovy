// https://mvnrepository.com/artifact/org.postgresql/postgresql
@Grapes(
    @Grab(group='org.postgresql', module='postgresql', version='42.7.3')
)
@GrabConfig(systemClassLoader = true)
import groovy.sql.Sql

def url = 'jdbc:postgresql://localhost:5432/groovy_db'
def usr = 'groovy_usr'
def pwd = 'groovy_pwd'
def drv = 'org.postgresql.Driver'

def db  = Sql.newInstance(url, usr, pwd, drv)
try (db) {
    def SQL = 'select * from messages'
    println '-- eachRow --'
    db.eachRow(SQL) {/*groovy.sql.GroovyResultSet*/ row ->
        System.out.printf '%d) %s%n', row.id, row.msg
    }
    
     println '-- firstRow --'
     def row = db.firstRow(SQL)
     System.out.printf '%d) %s%n', row.id, row.msg
    
    println '-- rows --'
    def rows = db.rows(SQL)
    rows.each {println it}
    
    println '-- query --'
    db.query(SQL) {/*java.sql.ResultSet*/ rs ->
        while (rs.next()) {
            System.out.printf '%d) %s%n', rs.getInt('id'), rs.getString('msg')
        }
    }
}
''