def url  = 'https://www.techrepublic.com/rssfeeds/articles/'
println new XmlParser().parse(url).channel.item.collect {
    def data = [it.title, it.pubDate, it.description, it.link]
    '*** ' + data*.text().join('\n    ') + '\n'
}.take(4).join('\n')
''


