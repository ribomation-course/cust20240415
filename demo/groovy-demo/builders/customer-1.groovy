class Book {String title; float price}
class Invoice {Date order; List books=[]}
class Customer{String name; List invoices=[]}

void dump(Customer c) {
    printf 'Customer: %s%n', c.name
    c.invoices
     .sort {a,b -> a.order <=> b.order}
     .each {it -> 
       println "* Order: ${it.order.format('d MMMM yyyy')}"
       it.books
         .sort {a,b -> a.price <=> b.price}
         .each {book -> printf '  - %s: %.2f kr%n', book.title, book.price}
    }
    printf 'Total: %.2f kr%n', c.invoices*.books*.price.sum().sum()
}

def r = new Random()
def c = new Customer(name:'Anna Conda')
def i = new Invoice(order:new Date()-r.nextInt(400))
c.invoices << i
i.books << new Book(title:'Groovy Primer', price:100+r.nextInt(200))
i.books << new Book(title:'Groovy Tutorial', price:100+r.nextInt(200))

i = new Invoice(order:new Date()-r.nextInt(45))
c.invoices << i
i.books << new Book(title:'Java Basics', price:100+r.nextInt(200))
i.books << new Book(title:'C++ Primer', price:100+r.nextInt(200))
i.books << new Book(title:'Rust for Dummies', price:100+r.nextInt(200))

dump(c)