def docker(Closure body) {
    def params = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = params
    body()
    
    println "pulling docker image '${params.image}:${params.tag} (${params.os}/${params.arch})'"
    if (params.progress) {
        (1..25).each { print '.'; sleep 250 }
        def r = new Random()
        def mb = Math.abs(25 + 50*r.nextGaussian())
        printf '\ndownloaded %.2f MB%n', mb
    }
}

// -- pipeline config --
docker {
    image = 'nginx'
    tag = 'alpine3.18-perl'
    os = 'linux'
    arch = 'amd64'
    progress = true
}