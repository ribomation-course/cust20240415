import groovy.xml.MarkupBuilder

def names = ['Anna', 'Berit', 'Carin', 'Doris']
def r     = new Random()

def buf = new StringWriter()
def xml = new MarkupBuilder(buf)
xml.'person-list' {
    names.eachWithIndex {n, k ->
        person(id: k+1) {
            name type: String.class.name, "${n} ${n}son"
            age(int: r.nextInt(60) + 20)
            weight float: Math.abs(15 * r.nextGaussian() + 60).round(1)
            female(bool: true)
        }
    }
}

println buf
