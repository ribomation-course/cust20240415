import groovy.xml.StreamingMarkupBuilder

def names = ['Anna', 'Berit', 'Carin', 'Doris']
def r     = new Random()


def xml      = new StreamingMarkupBuilder()
def writable = xml.bind {
    'person-list' {
        names.eachWithIndex {n, k ->
            person(id: k+1) {
                name type: String.class.name, "${n} ${n}son"
                age(int: r.nextInt(60) + 20)
                weight float: Math.abs(15 * r.nextGaussian() + 60).round(1)
                female(bool: true)
            }
        }
    }
}

def buf = new StringWriter()
writable.writeTo(buf)
println buf
