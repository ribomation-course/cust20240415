import groovy.xml.XmlSlurper

def payload = '''
<books>
  <book isbn="978-1935182443">
    <title>Groovy in Action, 2nd ed.</title>
    <author>Dierk König</author>
  </book>
  <book isbn="978-0978739294">
    <title>Groovy Recipes: Greasing the Wheels of Java</title>
    <author>Scott Davis</author>
  </book>
  <book isbn="978-1484250570">
    <title>Learning Groovy 3: Java-Based Dynamic Scripting</title>
    <author>Adam L. Davis</author>
  </book>
</books>
'''

def xml  = new XmlSlurper()
def root = xml.parseText(payload)
println 'root.class: ' + root.getClass().name
root.book*.title.each { println "* ${it}" }
println '----'
root.book.each {b -> 
    printf '# %s, %s (%s)%n', b.title.toString()[0..15], b.author, b.@isbn
}
''