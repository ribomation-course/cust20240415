def time = '10|15|35'

(time.split(/\|/) as ArrayList).withIndex().collect {val, ix -> 
 switch (ix){
   case 0: return "${val}h"
   case 1: return "${val}m"
   case 2: return "${val}s"
 }
}
.join(' ')
