import groovy.json.JsonSlurper
import groovy.json.JsonParserType

def json = new JsonSlurper(type: JsonParserType.LAX)
def obj = json.parseText '''
{
    "name": "Anna Conda",
    "age": 42,
    "weight": 54.32,
    "female": true,
    "tags": ["groovy", "java", "jenkins"],
    "address": {
        "street": "42 Hacker Lane", 
        "postCode": 12345,
        "city": "Perl Lake"
    },
    "updatedAt": "2020-11-06T09:27:22+0000",
}
'''
println 'obj: ' + obj
println 'obj.class      : ' + obj.getClass().name
println 'name.class     : ' + obj.name.getClass().name
println 'age.class      : ' + obj.age.getClass().name
println 'female.class   : ' + obj.female.getClass().name
println 'weight.class   : ' + obj.weight.getClass().name
println 'updatedAt.class: ' + obj.updatedAt.getClass().name
