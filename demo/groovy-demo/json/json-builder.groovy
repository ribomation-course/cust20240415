import groovy.json.JsonBuilder

def bld = new JsonBuilder()
def json = bld.person {
    name 'Anna Conda'
    age 42
    female true
    address (
        street: '42 Hacker Lane',
        postCode: '123 456',
        city: 'Perl Lake'
    )
    tags 'groovy', 'java', 'perl'
    map 'a':1, 'b':2
    arr (
        10,
        20,
        30
    )
}
println 'person = ' + json.person
println 'address = ' + json.person.address
println 'tags = ' + json.person.tags
println 'class = ' + json.getClass()
