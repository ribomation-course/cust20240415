import groovy.json.JsonOutput

println 'text: ' + JsonOutput.toJson('Just a silly text')
println 'list: ' + JsonOutput.toJson([1,2,3,4])
println 'map: ' + JsonOutput.toJson([a:1, b:2, c:3])

class Person {String name; int age; boolean male}
println 'object: ' + JsonOutput.toJson(new Person(name:'Nisse', age:42, male:true))

def r = new Random()
def lst = ['Anna','Berit','Carin']
          .collect {new Person(name:it, age:20+r.nextInt(40), male:false)}
println 'objects: ' +  JsonOutput.prettyPrint( JsonOutput.toJson(lst) )
