import java.util.zip.GZIPInputStream
import groovy.json.JsonSlurper
import groovy.json.JsonParserType

def url = 'https://api.stackexchange.com/2.2/tags?site=stackoverflow&sort=popular&order=desc&pagesize=10'
def input = new GZIPInputStream(url.toURL().newInputStream())
def json = new JsonSlurper(type: JsonParserType.INDEX_OVERLAY)
def items = json.parse(input).items
items.collect { ([count:it.count, name:it.name]) }
     .each {pair -> 
         printf '%s: %d%n', pair.name.padRight(10), pair.count
     }
''