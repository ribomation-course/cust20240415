import groovy.json.StreamingJsonBuilder

def filename = './person.json'
def out = new FileWriter(filename)
def bld = new StreamingJsonBuilder(out)
def json = bld.person {
    name 'Anna Conda'
    age 42
    female true
    address (
        street: '42 Hacker Lane',
        postCode: '123 456',
        city: 'Perl Lake'
    )
    tags 'groovy', 'java', 'perl'
    map 'a':1, 'b':2
    arr (
        10,
        20,
        30
    )
}
out.close()
print new File(filename).text
