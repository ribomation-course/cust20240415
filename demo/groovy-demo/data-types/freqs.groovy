def txt = new File('./data.txt').text
def freqs = [:]
txt.split(/[\s.,']+/).each {it -> freqs[it] = 1 + (freqs[it] ?: 0)}
freqs = freqs.sort { -it.value }
freqs.each {word, count ->
  printf '%s: %d%n', word, count
}
''