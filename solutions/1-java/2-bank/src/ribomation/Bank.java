package ribomation;

public class Bank {
    private final String name;
    private final Account[] accounts;
    private int lastIndex = 0;

    public Bank(String name, int numAccounts) {
        this.name = name;
        this.accounts = new Account[numAccounts];
    }

    public void add(Account account) {
        if (lastIndex < accounts.length) {
            accounts[lastIndex++] = account;
        }
    }

    public int count() {
        return accounts.length;
    }

    @Override
    public String toString() {
        String result = String.format("--- %s ---%n", name);
        var totalBalance = 0.0;
        for (Account a : accounts) {
            result += String.format("%s%n", a);
            totalBalance += a.getBalance();
        }
        result += String.format("=============%n%10.2f kr%n", totalBalance);
        result += "-- o0o --";
        return result;
    }
}
