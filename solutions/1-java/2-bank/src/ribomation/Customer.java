package ribomation;

public class Customer {
    private final String name;
    private final Address addr;

    public Customer(String name, Address address) {
        this.name = name;
        this.addr = address;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", name, addr);
    }
}
