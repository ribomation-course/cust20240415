package ribomation;

public class Account {
    private double balance;
    private final Customer customer;

    public Account(double balance, Customer customer) {
        this.balance = balance;
        this.customer = customer;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return String.format("%10.2f kr, %s", balance, customer);
    }
}
