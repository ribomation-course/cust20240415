package ribomation;

public class Address {
    private final String street, postCode, city;

    public Address(String street, String postCode, String city) {
        this.street = street;
        this.postCode = postCode;
        this.city = city;
    }

    @Override
    public String toString() {
        return String.format("%s, %s %s", street, postCode, city);
    }
}
