package ribomation;

import java.util.Random;

public class App {
    public static void main(String[] args) {
        var app = new App();
        var n = args.length == 0 ? 5 : Integer.parseInt(args[0]);
        app.create(n);
        app.populate();
        app.dump();
    }

    Bank bank;

    void create(int n) {
        bank = new Bank("Ebberods Bank", n);
    }

    void populate() {
        var r = new Random();
        for (int k = 1; k <= bank.count(); k++) {
            var addr = new Address("street-" + k, "zip-" + k, "city-" + k);
            var cust = new Customer("Nisse-" + k, addr);
            var acc = new Account(r.nextGaussian() * 250 + 500, cust);
            bank.add(acc);
        }
    }

    void dump() {
        System.out.println(bank);
    }
}
