package ribomation;

import java.util.StringJoiner;

public class Person {
    private final String firstName;
    private final String lastName;
    private final int age;
    private final String city;
    private final String country;

    public Person(String firstName, String lastName, int age, String city, String country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.city = city;
        this.country = country;
    }

    public static Person fromCSV(String csv, String sep) {
        var fields = csv.split(sep);
        var ix = 0;
        var fn = fields[ix++];
        var ln = fields[ix++];
        var age = Integer.parseInt(fields[ix++]);
        var cty = fields[ix++];
        var ctry = fields[ix++];
        return new Person(fn, ln, age, cty, ctry);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("age=" + age)
                .add("city='" + city + "'")
                .add("country='" + country + "'")
                .toString();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}
