package ribomation;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        app.load("./data/person-data.csv");
        app.printYoungsters();
        app.saveRetirees();
    }

    private final List<Person> objs = new ArrayList<>();

    private void load(String csvFilename) throws IOException {
        var lines = Files.readAllLines(Path.of(csvFilename));
        var first = true;
        for (String line : lines) {
            if (first) {
                first = false;
            } else {
                objs.add(Person.fromCSV(line, ","));
            }
        }
    }

    private void printYoungsters() {
        System.out.println("-- Youngsters --");
        var cnt = 0;
        for (Person person : objs) {
            if (person.getAge() <= 30) {
                System.out.println(person);
                ++cnt;
            }
        }
        System.out.printf("found %d person objects%n", cnt);
    }

    private void saveRetirees() throws IOException {
        System.out.println("-- Retirees --");
        var buf = new StringBuilder();
        var cnt = 0;
        for (Person person : objs) {
            if (person.getAge() >= 65) {
                //System.out.println(person);
                buf.append(person).append(System.lineSeparator());
                ++cnt;
            }
        }
        var outfile = Path.of("./data/retirees.csv");
        Files.writeString(outfile, buf.toString());
        System.out.printf("written %s, %d chars, %d lines%n",
                outfile, Files.size(outfile), cnt);
    }
}
