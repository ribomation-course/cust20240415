package ribomation;

public class ClassicCar {
    private String model;
    private int year;
    public ClassicCar(String model, int year) {
        this.model=model;
        this.year=year;
    }
    public String toString() {
        return String.format("Car{%s, %d}", model, year);
    }
}
