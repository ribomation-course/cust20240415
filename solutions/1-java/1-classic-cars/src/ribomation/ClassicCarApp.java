package ribomation;
import java.util.Random;

public class ClassicCarApp {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("usage: java ...");
            return;
        }

        var r = new Random();
        var cnt=1;
        for (var model : args) {
            var year = 1950 + r.nextInt(30);
            var c = new ClassicCar(model, year);
            System.out.printf("%d) %s%n", cnt++, c);
        }
    }
}
