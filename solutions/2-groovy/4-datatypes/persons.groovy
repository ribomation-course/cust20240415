@groovy.transform.ToString
class Person {
    String name
    int    age
}
def r = new Random()
def lst = ['Anna','Bertil','Carin','Dan'].collect {
    new Person(name:it, age:20 + r.nextInt(60))
}
for (p in lst) println p

println ('-' * 20)
int N = 10_000
println ((1..N).sum())
System.out.printf '1+2+...+%d = %s (%s)%n', N, (1..N).sum(), N*(N+1)/2
''