import groovy.xml.XmlParser
def url   = 'https://www.techrepublic.com/rssfeeds/articles'
def nodes = new XmlParser().parse(url).channel.item
def news  = nodes
    .collect {
        def data = [it.title, it.pubDate, it.description, it.link]
        '*** ' + data*.text().join('\n    ') + '\n'
    }
    .take(4)
    .join('\n')
println news
