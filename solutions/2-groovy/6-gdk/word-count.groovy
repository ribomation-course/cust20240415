import groovy.io.FileType

def dir = new File("../../..")
//println "scanning ${dir.canonicalPath}"

def files = []
int totFiles = 0
dir.eachFileRecurse(FileType.FILES) { file ->
    ++totFiles
    def dotIdx = file.name.lastIndexOf('.')
    if (dotIdx < 0) return
    def ext = file.name.substring(dotIdx)
    if (['.groovy', '.java'].contains(ext) ) {
        files << file
    }    
}
println "found ${files.size()} groovy/java files of ${totFiles} in total"
//files.each { println it.path }

def result = files
    *.text
    *.split(/[^a-zA-Z]+/)
    .flatten()
    .findAll {it.size() > 2}
    .countBy {it}
    .sort {a,b -> b.value <=> a.value}
    .take(25)
    .each {word, count -> System.out.printf '%-10s: %3d%n', word, count}

''
