import groovy.xml.XmlParser

def url = 'https://repo1.maven.org/maven2/org/springframework/batch/spring-batch-core/5.1.1/spring-batch-core-5.1.1.pom'
def xml = new XmlParser().parse(url.toURL().newInputStream())
xml.dependencies.dependency.eachWithIndex {d,ix ->
    def group    = d.groupId.text()
    def artifact = d.artifactId.text()
    def version  = d.version.text()
    def scope    = d.scope.text()
    if (scope != 'compile') return 
    System.out.printf '%2d) %s:%s:%s%n', ix+1, group, artifact, version
}

''