import groovy.transform.*

@Immutable
@Sortable
@EqualsAndHashCode
@ToString(includeNames=true)
class Product {
    String name
    int    price
}


println '--- immutable ---'
try { 
    def q = new Product(name:'Book', price:42)
    q.price *= 10 
}
catch (x) {  println 'ooops: '+x }

println '--- equals & hashCode ---'
def p1 = new Product(name:'Book', price:42)
def p2 = new Product(name:'Book', price:42)
System.out.printf 'p1.equals(p2): %b%n', p1.equals(p2)
System.out.printf 'p1 == p2     : %b%n', (p1 == p2)
System.out.printf 'p1# == p2#   : %b (%d)%n', (p1.hashCode() == p2.hashCode()), p1.hashCode()
System.out.printf 'p1.is(p2)    : %b%n', p1.is(p2)
System.out.printf 'p1 === p2    : %b%n', (p1 === p2) //groovy v3
System.out.printf 'p1 === p1    : %b%n', (p1 === p1) //groovy v3


@Singleton
class Registry {
    Set<Product> items = new TreeSet<>()
    void add(p) { items << p }    
}

println '--- sortable ---'
def names = ['Apple','Banana','Coco Nut','Date Plum','Orange','Kiwi']
def r     = new Random()
25.times {
    def name  = names[r.nextInt(names.size())]
    def price = 10 + r.nextInt(50)
    def prod  = new Product(name:name, price:price)
    Registry.instance.add(prod)
}
Registry.instance.items.eachWithIndex {p,ix -> 
    System.out.printf '%d) %s%n', ix+1, p
}


''