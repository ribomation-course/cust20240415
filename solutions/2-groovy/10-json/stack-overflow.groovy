import java.util.zip.GZIPInputStream
import groovy.json.JsonSlurper
import groovy.json.JsonParserType

def N     = 25
def url   = 'https://api.stackexchange.com/2.2/tags?site=stackoverflow&sort=popular&order=desc&pagesize=' + N
def input = new GZIPInputStream( url.toURL().newInputStream() )
def json  = new JsonSlurper(type: JsonParserType.INDEX_OVERLAY)
def reply = json.parse(input)

def SWEDISH = new Locale('sv')
reply.items.each {
    System.out.printf SWEDISH, '%-14s: %,10d%n', it.name, it.count
}

''