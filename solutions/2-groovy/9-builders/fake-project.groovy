def dir = new File('./project')
if (dir.exists()) { dir.deleteDir() }
dir.mkdirs()

def bld = new FileTreeBuilder(dir)
bld {
    'pom.xml' "<xml>nothing here</xml>\n"
    src {
        main {
            groovy {
                'hello.groovy' "println 'Hello from a Groovy project'\n"
            }
            resources {
                'message.txt' "Just a silly text\n"
            }
        }
    }
    target {
        classes {
            'hello.clazz' "abc123 just some binary stuff\n"
        }
    }
}

dir.eachFileRecurse { f ->
    if (f.isFile()) {
        System.out.printf '%s: "%s"%n', f, f.text.trim()
    }    
}

''