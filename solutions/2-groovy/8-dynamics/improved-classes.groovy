println '-- toSnakeCase() --'
java.lang.String.metaClass.toSnakeCase = {->
    delegate.split(/\s+/)*.capitalize().join('_')
}

def s = 'foobar strikes again'
System.out.printf '"%s" --> "%s"%n', s, s.toSnakeCase()

println '\n-- asISO8601 --'
java.util.Date.metaClass.getAsISO8601 = {->
    delegate.format('yyyy-MM-dd HH:mm:ss')
}

def d = new Date()
System.out.printf '"%s" --> "%s"%n', d, d.asISO8601

''