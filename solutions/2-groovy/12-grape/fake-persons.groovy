@Grapes(
    @Grab(group='org.jeasy', module='easy-random-core', version='5.0.0', scope='test')
)
import org.jeasy.random.EasyRandom
import groovy.transform.*

@ToString
@Sortable
class Person {
    String firstName
    String lastName
    int    age
    String city
}

def g = new EasyRandom()
def persons = (1..10).collect {
    g.nextObject(Person.class)
}

persons.each {println it}
''