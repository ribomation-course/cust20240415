import java.util.Random

class Product {
    String name
    float  price
    int    count=1
    Date   date=new Date()
    
    String getName() {name.toUpperCase()}
    float  getPrice() {price * 1.25}
    String toString() {"Product{${getName()}, ${String.format '%.2f kr', getPrice()}, ${count} in store, created ${date.format('yyyy-MM-dd, HH:mm:ss')}}"}
}

def names = ['apple','banana','coco nut', 'date plum']
def r = new Random()
def products = names.collect {
    new Product(name: it, count: r.nextInt(10), price: Math.abs(r.nextGaussian()*100), date: new Date() - r.nextInt(30))
}
for (var p : products) println p
''