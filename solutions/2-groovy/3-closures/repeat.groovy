def repeat(int n, Closure f) {
    (1..n).each { f(it) }
}

repeat(5) { 
    println "${it}) Groovy is cool!" 
}
''