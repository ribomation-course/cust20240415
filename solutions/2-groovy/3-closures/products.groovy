
def name(r)  { "prod-" +  Math.abs(r.nextInt()) }
def price(r) { Math.abs(500 + 100*r.nextGaussian()) }
def count(r) { r.nextInt(16) }
def date(r)  { new Date() - r.nextInt(1000) }


class Product {
    String name
    float  price
    int    count
    Date   date
    String toString() {
        String.format 'Product{%s, %.2f kr, %d in stock, inserted %tF}', name,price,count,date
    }
}

def r = new Random(System.nanoTime())
int N = 1000
def products = (1..N).collect {
    new Product(name:name(r), price:price(r), count:count(r), date:date(r))
}

println '-- smallest price, greater than 600kr --'
def lst1 = products.findAll {it.price > 600}.sort { it.price }
println lst1 ? lst1[0] : 'none found'

println '-- average price of products out of stock --'
def lst2 = products.findAll {it.count == 0}.collect {it.price}
if (lst2) {
     System.out.printf '%.2f kr%n', lst2.sum() / lst2.size() 
} else { 
    println 'none found' 
}
 
println '-- Sort in descending date order and print out the 5 first  --'
def lst3 = products.sort { it.date }.reverse().take(5)
lst3.each {println it}
''