class Product {
    String name
    float price
    String toString() {String.format 'Product(%s, %.2f kr)', name, price}
}
def r = new Random()
def products = [
    'Apple', 'Banana', 'Coco Nut', 'Date Plum', 'Orange'
].collect {
    new Product(name:it, price:10 + Math.abs(100 * r.nextGaussian()))
}

println '-- average price --'
println String.format('%.2f kr', products*.price.sum() / products.size())

println '-- desc price order --'
products.sort {a,b -> b.price <=> a.price}.each { println it }
''