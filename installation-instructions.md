# Installation Instructions

In order to participate and perform the programming exercises of the course,
you need to have the following installed.

## Operating System (_any of_)
* MacOS
* Linux
* Ubuntu @ WSL2 @ Windows11

If you are using Windows, you must have WSL version 2 installed,
plus Ubuntu Linux installed.
* https://learn.microsoft.com/en-us/windows/wsl/install
* https://www.windowscentral.com/how-install-wsl2-windows-10
* https://linuxsimply.com/linux-basics/os-installation/wsl/ubuntu-on-wsl2/


## Tools
* SDKman
  - https://sdkman.io/install
  - `curl -s "https://get.sdkman.io" | bash`
  - `source "$HOME/.sdkman/bin/sdkman-init.sh"`
  - `sdk version`
* Java JDK 22
  - `sdk install java 22-open`
* Groovy 4
  - `sdk install groovy 4.0.20`
* Jenkins
  - https://www.jenkins.io/download/
  - https://www.jenkins.io/download/lts/macos/
* GIT Client
    - https://git-scm.com/downloads
    - https://git-scm.com/download/mac
* Decent IDE, such as
    * MicroSoft Visual Code
        - https://code.visualstudio.com/
* BASH terminal
* Modern web browser

## Accounts
* GitHub (_free_) account
    * https://github.com/signup

